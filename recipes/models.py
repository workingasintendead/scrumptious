from django.db import models

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    rating = models.CharField(max_length=4)
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.title
