from django.shortcuts import render, get_object_or_404
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from recipes.models import Recipe

# Create your views here.
def show_recipe(request: HttpRequest, id: int) -> HttpResponse:
    recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe_object": recipe}
    # every key in the context dictionary is a variable in the HTML template.
    return render(request, "recipes/detail.html", context)
